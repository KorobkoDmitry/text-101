﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour {

    public Text text;
    private enum States {mirror, cell, sheets_0, lock_0, cell_mirror, sheets_1, lock_1, freedom};
    private States myState;

	// Use this for initialization
	void Start () {
        myState = States.cell;
	}
	
	// Update is called once per frame
	void Update () {
        print(myState);
             if (myState == States.cell)        {Cell();}
        else if (myState == States.sheets_0)    {Sheets0();}
        else if (myState == States.mirror)      {Mirror();}
        else if (myState == States.lock_0)      {Lock0();}
        else if (myState == States.sheets_1)    {Sheets1();}
        else if (myState == States.cell_mirror) {CellMirror();}
        else if (myState == States.lock_1)      {Lock1();}
        else if (myState == States.freedom)     {Freedom();}
    }

    void Cell()
    {
        text.text = "You are in a prison cell, and you want to escape. There are some dirty sheets on " +
                    "the bed, a mirror on the wall, and the door the locke from the outside.\n\n" +
                    "Press S to view sheets, M to view a mirror, or L to view lock.";

             if (Input.GetKeyDown(KeyCode.S)) {myState = States.sheets_0;}
        else if (Input.GetKeyDown(KeyCode.M)) {myState = States.mirror;}
        else if (Input.GetKeyDown(KeyCode.L)) {myState = States.lock_0;}
    }

    void Sheets0()
    {
        text.text = "You can't belive you sleep in these things. surelly it's time somebody " +
                    "changed them. The pleasures of prison life I guess!\n\n" +
                    "Press R to return to roaming your cell.";

        if (Input.GetKeyDown(KeyCode.R)) {myState = States.cell;}
    }

    void Mirror()
    {
        text.text = "The dirty old mirror on the wall seems loose.\n\n" +
                    "Press T to take mirror, or R to return to your cell..";

             if (Input.GetKeyDown(KeyCode.T)) {myState = States.cell_mirror;}
        else if (Input.GetKeyDown(KeyCode.R)) {myState = States.cell;}
    }

    void Lock0()
    {
        text.text = "This is one of those button lock. You have not idea what the combination is. You " +
                    "wish you could somehow see where the dirty fingerprints were, maybe that would help.\n\n" +
                    "Press R to return to roaming your cell.";

        if (Input.GetKeyDown(KeyCode.R)) {myState = States.cell;}
    }

    void Sheets1()
    {
        text.text = "Holding a mirror in your hand doesn't make the sheets look any better.\n\n" +
                    "Press R to return to cell mirror.";

        if (Input.GetKeyDown(KeyCode.R)) {myState = States.cell_mirror;}
    }

    void CellMirror()
    {
        text.text = "You are still on your cell, and you still want to escape! There are some dirty sheets on the " +
                    "bed, a mark where the mirror was, and that pesky door is still there, and firmly locked!\n\n" +
                    "Press S to view sheets,  or L to view lock.";

             if (Input.GetKeyDown(KeyCode.S)) {myState = States.sheets_1;}
        else if (Input.GetKeyDown(KeyCode.L)) {myState = States.lock_1;}
    }

    void Lock1()
    {
        text.text = "You carefully put the mirror trough the bars, and turn it round so you can see the lock. You can " +
                    "just make out fingerprints around the buttons. You press the dirty buttons, and hear a click.\n\n" +
                    "Press O to open or R to return to cell mirror.";

             if (Input.GetKeyDown(KeyCode.O)) {myState = States.freedom;}
        else if (Input.GetKeyDown(KeyCode.R)) {myState = States.cell_mirror;}
    }

    void Freedom()
    {
        text.text = "You are free!.\n\n" +
                    "Press P to play again.";

        if (Input.GetKeyDown(KeyCode.P)) {myState = States.cell;}
    }
}
